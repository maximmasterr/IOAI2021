# Azamat Bayramov

from src.bot import IBot
from src.geometry import Direction, Coordinate, directions
from src.snake import Snake


def create_map(snake, opponent, mazeSize):
    lst = [[0 for s in range(mazeSize.x)] for i in range(mazeSize.x)]
    for i in snake.body:
        lst[i.y][i.x] = 1
    for i in opponent.body:
        lst[i.y][i.x] = 1

    return lst


def get_good_coords(map_lst):
    color = 2
    while True:
        colored = False

        color_coords = []

        for i in range(len(map_lst)):
            for s in range(len(map_lst[i])):
                if map_lst[i][s] == 0:
                    map_lst[i][s] = color
                    color_coords.append((i, s))
                    colored = True
                    break
            if colored:
                break

        if not color_coords:
            break

        while color_coords:
            s = color_coords.pop(0)
            y, x = s
            #print(x, y)
            if y > 0:
                if map_lst[y - 1][x] == 0:
                    map_lst[y - 1][x] = color
                    color_coords.append((y - 1, x))
            if y < len(map_lst) - 1:
                if map_lst[y + 1][x] == 0:
                    map_lst[y + 1][x] = color
                    color_coords.append((y + 1, x))
            if x > 0:
                if map_lst[y][x - 1] == 0:
                    map_lst[y][x - 1] = color
                    color_coords.append((y, x - 1))
            if x < len(map_lst) - 1:
                if map_lst[y][x + 1] == 0:
                    map_lst[y][x + 1] = color
                    color_coords.append((y, x + 1))

        color += 1

    color_dict = {}

    for i in map_lst:
        for s in i:
            if s in color_dict.keys():
                color_dict[s] += 1
            else:
                color_dict[s] = 1

    return map_lst, color_dict


def get_occupied_cells(snake, opponent, apple):
    lst = []

    lst += snake.body

    if snake.body[0].getDistance(apple) > 1:
        lst.pop(-1)

    lst += opponent.body

    if opponent.body[0].getDistance(apple) > 1:
        lst.pop(-1)

    return lst


def check_move(direction, snake_head: Coordinate, occupied_cells, mazeSize):
    snake_head = snake_head.moveTo(direction)

    if snake_head.x < 0 or snake_head.x >= mazeSize.x:
        return False
    if snake_head.y < 0 or snake_head.y >= mazeSize.x:
        return False

    if snake_head in occupied_cells:
        return False

    return True


def get_good_moves(snake_head: Coordinate, occupied_cells, mazeSize):
    lst = []
    for direction in directions:

        if check_move(direction, snake_head.clone(), occupied_cells, mazeSize):
            lst.append(direction)

    return lst


def get_best_move(good_moves, snake_head, apple, map_lst, snake_len):
    start_good_moves = good_moves.copy()
    if not good_moves:
        good_moves = directions

    color_map, color_dict = get_good_coords(map_lst)

    colors = list(color_dict.keys())
    colors.remove(1)

    if len(colors) > 1:
        colors.sort(key=lambda s: color_dict[s])
        move_colors = []

        for move in good_moves:
            coord = snake_head.moveTo(move)
            color = color_map[coord.y][coord.x]
            move_colors.append(color)
        new_good_moves = good_moves.copy()

        for move in good_moves:
            coord = snake_head.moveTo(move)
            color = color_map[coord.y][coord.x]
            if color_dict[color] < snake_len:
                new_good_moves.remove(move)

        good_moves = new_good_moves

        if not good_moves:
            good_moves = start_good_moves

    best_move = False
    best_move_n = 10000

    for move in good_moves:
        coord = snake_head.moveTo(move)
        n = coord.getDistance(apple)
        if n < best_move_n:
            best_move = move
            best_move_n = n

    return best_move


class Bot(IBot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def chooseDirection(self, snake: Snake, opponent: Snake, mazeSize: Coordinate,
                        apple: Coordinate) -> Direction:

        map_lst = create_map(snake, opponent, mazeSize)

        snake_head = snake.body[0]
        opponent_head = opponent.body[0]

        occupied_cells = get_occupied_cells(snake, opponent, apple)

        good_moves = get_good_moves(snake_head, occupied_cells, mazeSize)
        good_opponent_moves = get_good_moves(opponent_head, occupied_cells, mazeSize)

        best_move = get_best_move(good_moves, snake_head, apple, map_lst, len(snake.body))
        best_opponent_move = get_best_move(good_opponent_moves, opponent_head, apple, map_lst,
                                           len(opponent.body))

        coord_after_move = snake_head.moveTo(best_move)
        coord_after_opponent_move = opponent_head.moveTo(best_opponent_move)

        if coord_after_move == coord_after_opponent_move:
            if coord_after_move == coord_after_opponent_move == apple:
                if len(snake.body) == len(opponent.body):
                    sum1 = snake.body[0].x * 100 + snake.body[0].y
                    sum2 = opponent.body[0].x * 100 + opponent.body[0].y
                    if sum1 > sum2:
                        return best_move
            if len(snake.body) > len(opponent.body):
                return best_move
            else:
                if len(good_moves) == 1:
                    return best_move
                else:
                    good_moves.remove(best_move)
                    return get_best_move(good_moves, snake_head, apple, map_lst, len(snake.body))

        return best_move
