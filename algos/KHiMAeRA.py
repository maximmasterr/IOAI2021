
from src.bot import IBot
from src.geometry import Direction, Coordinate, RIGHT
from src.snake import Snake
import random
MOVES_BEFORE_DIE = 180

class Bot(IBot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.i = 0
        self.N_STEPS = 3
     
    def check_move(self, act: Coordinate, pl_1: Snake, pl_2: Snake):
        if act in pl_1.body:
            return False
        if act in pl_2.body:
            return False
        if act.x < 0 or act.x > 13:
            return False
        if act.y < 0 or act.y > 13:
            return False
        return True
    
    def poss_moves(self, pl_1: Snake, pl_2: Snake):
        head = pl_1.body[0]
        moves = [Coordinate(head.x, head.y+1), Coordinate(head.x, head.y-1), Coordinate(head.x+1, head.y), Coordinate(head.x-1, head.y)]
        return [act for act in moves if Bot.check_move(self, act, pl_1, pl_2)]
    
    def space(self, pl_1: Snake, pl_2: Snake, apple: Coordinate):
        vec = [Coordinate(0, 1), Coordinate(0, -1), Coordinate(1, 0), Coordinate(-1, 0)]
        q = set()
        used = set()
        coor = pl_1.body[0]
        q.add((coor, 0))
        while len(q) > 0:
            t = q.pop()
            coor = t[0]
            cnt = t[1]
            if coor == apple:
                return cnt
            used.add(coor)
            for v in vec:
                d = Coordinate(coor.x + v.x, coor.y + v.y)
                if d in used:
                    continue
                if Bot.check_move(self, d, pl_1, pl_2) == False:
                    continue
                cnt+=1
                q.add((d, cnt))
                cnt-=1
                
        return 1e9
    
    def evaluation(self, pl_1: Snake, pl_2: Snake, apple: Coordinate, moves_me: list, depth):
        moves_opp = Bot.poss_moves(self, pl_2, pl_1)
        MaxPoints = 100
        
        if len(moves_me) == 0:
            return -(MaxPoints+200)*(5)
        if len(moves_opp) == 0:
            return (MaxPoints+50)*(depth+1)
        
        
        
        if pl_1.body[0].getDistance(pl_2.body[0]) <= 2:
            #print("ALLERT!")
            if apple.getDistance(pl_1.body[0]) == apple.getDistance(pl_2.body[0]):
                if len(pl_1.body) < len(pl_2.body):
                    return -(MaxPoints+200)*(depth+1)
                elif len(pl_1.body) == len(pl_2.body):
                    return (-50)*(depth+1)
                else:
                    return (MaxPoints+100)*(depth+1)
                 
        
        if apple == pl_1.body[0] or apple in moves_me:
            return MaxPoints*(depth+1)
        min_dist = Bot.space(self, pl_1, pl_2, apple)
        if(min_dist == 1e9):
            return (5)*(MaxPoints - min_dist)
        return (depth+1)*(MaxPoints - min_dist)
        
    def negamax(self, depth: int, pl_1: Snake, pl_2: Snake, apple: Coordinate, color: int):
        valid_moves = Bot.poss_moves(self, pl_1, pl_2)
        
        if depth == 0 or len(valid_moves) == 0 or apple == pl_1.body[0]:
            return color*Bot.evaluation(self, pl_1, pl_2, apple, valid_moves, depth)
        
        val = -1e18
        for act in valid_moves:
            child = pl_1.clone()
            child.body[0] = act
            length = len(pl_1.body)
            for i in range(length-1):
                child.body[i+1] = pl_1.body[i]
            val = max(val, -Bot.negamax(self, depth-1, pl_2.clone(), child, apple, -color))
            
        
        return val
        
    def eval_move(self, act: Coordinate, pl_1: Snake, pl_2: Snake, apple: Coordinate, N_STEPS: int):
        child = pl_1
        child.body[0] = act
        length = len(pl_1.body)
        
        for i in range(length-1):
            child.body[i+1] = pl_1.body[i]
        score = Bot.negamax(self, N_STEPS-1, child, pl_2, apple, 1)
        return score
                      
    def chooseDirection(self, snake: Snake, opponent: Snake, mazeSize: Coordinate, apple: Coordinate) -> Direction:
        #print(self.i)
        self.i += 1
        head = snake.body[0]
        valid_moves = Bot.poss_moves(self, snake, opponent)
        
        scores = dict(zip(valid_moves, [Bot.eval_move(self, move, snake.clone(), opponent.clone(), apple, self.N_STEPS) for move in valid_moves]))
        max_moves = [key for key in scores.keys() if scores[key] == max(scores.values())]
        
        if self.i > MOVES_BEFORE_DIE or len(max_moves) == 0:

            firstBodyElement = snake.body[1]
            directionToDeath = head.getDirection(firstBodyElement)
            
            
            return directionToDeath
        else:
            move = random.choice(max_moves)
            dir_act = head.getDirection(move)
            #print(move, scores[move])
            return dir_act