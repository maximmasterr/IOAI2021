from src.bot import IBot
from src.geometry import Direction, Coordinate, RIGHT, LEFT, UP, DOWN
from src.snake import Snake

class Bot(IBot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cells = []
        self.ehead = []
        self.edgecost = 3

    def chooseDirection(self, snake: Snake, opponent: Snake, mazeSize: Coordinate, apple: Coordinate) -> Direction:
        def safe(self, c : Coordinate):
            if(c.x < 0 or c.y < 0 or c.x >= mazeSize.x or c.y >= mazeSize.y):
                return 0
            for i in self.cells:
                if(c == i):
                    return 0
            for i in self.ehead:
                if(c == i):
                    return 0
            return 1
        def safex(self, c : Coordinate):
            if(c.x < 0 or c.y < 0 or c.x >= mazeSize.x or c.y >= mazeSize.y):
                return 0
            for i in self.cells:
                if(c == i):
                    return 0
            for i in self.ehead:
                if(c == i):
                    return 1
            return 2
        def dist(a : Coordinate,b : Coordinate):
            return abs(a.x-b.x)+abs(a.y-b.y)
        """
        cur = []
        nxt = [[[],[]]]
        for a in range(self.prec):
            
        """
        
        self.cells = []
        head = snake.body[0]
        ehead = opponent.body[0]
        self.ehead = [Coordinate(ehead.x-1,ehead.y),Coordinate(ehead.x+1,ehead.y),Coordinate(ehead.x,ehead.y-1),Coordinate(ehead.x,ehead.y+1)]
        for i in range(len(snake.body)-1):
            self.cells = self.cells + [snake.body[i]]
        if(dist(apple,ehead) > 1):
            for i in range(len(opponent.body)-1):
                self.cells = self.cells + [opponent.body[i]]
        else:
            for i in range(len(opponent.body)):
                self.cells = self.cells + [opponent.body[i]]
        if((len(snake.body) % 2) == 0 and len(snake.body) > 7 and len(snake.body) < 31 and len(snake.body) > len(opponent.body)):
            cyc = (len(snake.body) // 2) - 3
            if(apple.x > 0 and apple.y > 0 and apple.x < mazeSize.x-1 and apple.y < mazeSize.y-1):
                stt = apple.x - cyc
                edd = apple.x + 1
                if(stt < 0):
                    edd -= stt
                    stt -= stt
                if(head.y == apple.y - 1):
                    if(head.x < edd and safex(self,Coordinate(head.x+1,head.y))):
                        return RIGHT
                    if(head.x > edd and safex(self,Coordinate(head.x-1,head.y))):
                        return LEFT
                    if(head.x == edd and safex(self,Coordinate(head.x,head.y+1))):
                        return UP
                if(head.y == apple.y):
                    if(head.x < apple.x and safex(self,Coordinate(head.x,head.y-1))):
                        return DOWN
                    if(head.x > apple.x and safex(self,Coordinate(head.x,head.y+1))):
                        return UP
                if(head.y == apple.y + 1):
                    if(head.x > stt and safex(self,Coordinate(head.x-1,head.y))):
                        return LEFT
                    if(head.x < stt and safex(self,Coordinate(head.x+1,head.y))):
                        return RIGHT
                    if(head.x == stt and safex(self,Coordinate(head.x,head.y-1))):
                        return DOWN
        rd = [[[-1,9999] for j in range(mazeSize.y)] for i in range(mazeSize.x)]
        rd[head.y][head.x] = [-1,0]
        ec = self.edgecost
        while True:
            con = 0
            for j in range(mazeSize.y):
                for i in range(mazeSize.x):
                    cs = 10
                    if(i < 1 or i > mazeSize.x - 2):
                        cs += 2
                    if(j < 1 or j > mazeSize.y - 2):
                        cs += 2
                    if(i < 2 or i > mazeSize.x - 3):
                        cs += 1
                    if(j < 2 or j > mazeSize.y - 3):
                        cs += 1
                    if(i < 4 or i > mazeSize.x - 5):
                        cs += 1
                    if(j < 4 or j > mazeSize.y - 5):
                        cs += 1
                    if(rd[j][i][1] < 9999):
                        if(j > 0 and rd[j-1][i][1] > rd[j][i][1] + cs and safe(self,Coordinate(i,j-1))):
                            con = 1
                            if(rd[j][i][0] == -1):
                                rd[j-1][i] = [DOWN,rd[j][i][1]+cs]
                            else:
                                rd[j-1][i] = [rd[j][i][0],rd[j][i][1]+cs]
                        if(j < mazeSize.y - 1 and rd[j+1][i][1] > rd[j][i][1] + cs and safe(self,Coordinate(i,j+1))):
                            con = 1
                            if(rd[j][i][0] == -1):
                                rd[j+1][i] = [UP,rd[j][i][1]+cs]
                            else:
                                rd[j+1][i] = [rd[j][i][0],rd[j][i][1]+cs]
                        if(i > 0 and rd[j][i-1][1] > rd[j][i][1] + cs and safe(self,Coordinate(i-1,j))):
                            con = 1
                            if(rd[j][i][0] == -1):
                                rd[j][i-1] = [LEFT,rd[j][i][1]+cs]
                            else:
                                rd[j][i-1] = [rd[j][i][0],rd[j][i][1]+cs]
                        if(i < mazeSize.x - 1 and rd[j][i+1][1] > rd[j][i][1] + cs and safe(self,Coordinate(i+1,j))):
                            con = 1
                            if(rd[j][i][0] == -1):
                                rd[j][i+1] = [RIGHT,rd[j][i][1]+cs]
                            else:
                                rd[j][i+1] = [rd[j][i][0],rd[j][i][1]+cs]
            if not con:
                break
        if(rd[apple.y][apple.x][0] == -1):
            if(len(snake.body) > len(opponent.body)):
                rd = [[[-1,9999] for j in range(mazeSize.y)] for i in range(mazeSize.x)]
                rd[head.y][head.x] = [-1,0]
                ec = self.edgecost
                while True:
                    con = 0
                    for j in range(mazeSize.y):
                        for i in range(mazeSize.x):
                            cs = 10
                            if(i < 1 or i > mazeSize.x - 2):
                                cs += 2
                            if(j < 1 or j > mazeSize.y - 2):
                                cs += 2
                            if(i < 2 or i > mazeSize.x - 3):
                                cs += 1
                            if(j < 2 or j > mazeSize.y - 3):
                                cs += 1
                            if(i < 4 or i > mazeSize.x - 5):
                                cs += 1
                            if(j < 4 or j > mazeSize.y - 5):
                                cs += 1
                            if(rd[j][i][1] < 9999):
                                if(j > 0 and rd[j-1][i][1] > rd[j][i][1] + cs and safex(self,Coordinate(i,j-1))):
                                    con = 1
                                    if(rd[j][i][0] == -1):
                                        rd[j-1][i] = [DOWN,rd[j][i][1]+cs]
                                    else:
                                        rd[j-1][i] = [rd[j][i][0],rd[j][i][1]+cs]
                                if(j < mazeSize.y - 1 and rd[j+1][i][1] > rd[j][i][1] + cs and safex(self,Coordinate(i,j+1))):
                                    con = 1
                                    if(rd[j][i][0] == -1):
                                        rd[j+1][i] = [UP,rd[j][i][1]+cs]
                                    else:
                                        rd[j+1][i] = [rd[j][i][0],rd[j][i][1]+cs]
                                if(i > 0 and rd[j][i-1][1] > rd[j][i][1] + cs and safex(self,Coordinate(i-1,j))):
                                    con = 1
                                    if(rd[j][i][0] == -1):
                                        rd[j][i-1] = [LEFT,rd[j][i][1]+cs]
                                    else:
                                        rd[j][i-1] = [rd[j][i][0],rd[j][i][1]+cs]
                                if(i < mazeSize.x - 1 and rd[j][i+1][1] > rd[j][i][1] + cs and safex(self,Coordinate(i+1,j))):
                                    con = 1
                                    if(rd[j][i][0] == -1):
                                        rd[j][i+1] = [RIGHT,rd[j][i][1]+cs]
                                    else:
                                        rd[j][i+1] = [rd[j][i][0],rd[j][i][1]+cs]
                    if not con:
                        break
            if(rd[apple.y][apple.x][0] == -1):
                if(safe(self,Coordinate(head.x+1,head.y))):
                    return RIGHT
                if(safe(self,Coordinate(head.x-1,head.y))):
                    return LEFT
                if(safe(self,Coordinate(head.x,head.y+1))):
                    return UP
                if(safe(self,Coordinate(head.x,head.y-1))):
                    return DOWN
                if(safex(self,Coordinate(head.x+1,head.y))):
                    return RIGHT
                if(safex(self,Coordinate(head.x-1,head.y))):
                    return LEFT
                if(safex(self,Coordinate(head.x,head.y+1))):
                    return UP
                if(safex(self,Coordinate(head.x,head.y-1))):
                    return DOWN
                return RIGHT
        return rd[apple.y][apple.x][0]