from src.bot import IBot
from src.geometry import Direction, Coordinate, LEFT,RIGHT,UP,DOWN
from src.snake import Snake
import random
import copy

# Reduce this for faster execution
# Don't touch this, it works fine in all the games i tested it with.
num_moves_ahead = 4

MAX_DEPTH = (num_moves_ahead*2)-1
w1 = 500  #Weightage for points
w2 = 10  #Weightage for distance to apple
w3 = 100000  #Weightage for death
w4 = 50  #Weightage for my order

def minimax(depth,board,maximizingPlayer,alpha,beta):
    if (board.snake_dead or board.opp_dead) and (depth%2 == 0):
        return board.eval_position()

    if depth == (MAX_DEPTH+1):
        return board.eval_position()
  
    if maximizingPlayer:
        best = -1000000000
        moves = board.get_snake_moves()
        #print("At Depth",depth,"Possible Moves:",moves)
        for move in moves:
            cur_board = copy.deepcopy(board)
            cur_board.snake_move(move)
            val = minimax(depth+1,cur_board,False,alpha,beta)
            best = max(best,val)
            alpha = max(alpha,best)
            if beta <= alpha:
                break
        return best
    else:
        best = 1000000000;
        moves = board.get_opp_moves()
        #print("At Depth",depth,"Possible Moves:",moves)
        for move in moves:
            cur_board = copy.deepcopy(board)
            cur_board.opp_move(move)
            val = minimax(depth+1,cur_board,True,alpha,beta)
            best = min(best,val)
            beta = min(beta,best)
            if beta <= alpha:
                break 
        return best

class Board:
    def __init__(self,snake,opponent,points,opp_points,apple):
        self.snake = snake
        self.opponent = opponent
        self.points = points
        self.opp_points = opp_points
        self.apple = apple
        self.snake_dead = False
        self.opp_dead = False

    def eval_position(self):
        v1 = w1*(self.points-self.opp_points)
        v2 = w2*(self.opponent.body[0].getDistance(self.apple)-self.snake.body[0].getDistance(self.apple))
        v3 = w3*(int(self.opp_dead)-int(self.snake_dead))
        return v1+v2+v3

    def get_snake_moves(self):
        snake = self.snake
        opponent = self.opponent
        apple = self.apple

        head = snake.body[0]
        opp_head = opponent.body[0]
        opts = []
        if head.x < apple.x:
            opts.append(RIGHT)
        elif head.x > apple.x:
            opts.append(LEFT)
        if head.y < apple.y:
            opts.append(UP)
        elif head.y > apple.y:
            opts.append(DOWN)
        if len(opts) == 2:
            if (apple.x < head.x and head.x < opp_head.x) or (apple.x > head.x and head.x > opp_head.x):
                opts = opts[::-1]
            else:
                random.shuffle(opts)
        dirs = [LEFT,RIGHT,UP,DOWN]
        for dir in dirs:
            if dir not in opts:
                opts.append(dir)
        dirstodeath = []
        for piece in (snake.body+opponent.body):
            if piece != head:
                if (abs(piece.x-head.x)+abs(piece.y-head.y)) == 1:
                    dirstodeath.append(head.getDirection(piece))
        if head.x == 13:
            dirstodeath.append(RIGHT)
        elif head.x == 0:
            dirstodeath.append(LEFT)
        if head.y == 13:
            dirstodeath.append(UP)
        elif head.y == 0:
            dirstodeath.append(DOWN)
        # Avoiding snake collision at apple
        if head.getDirection(apple) != None:
            if opp_head.getDirection(apple) != None:
                if self.points < self.opp_points:
                    opts = opts[::-1]
        # Avoiding potential collision
        if (abs(head.x-opp_head.x)+abs(head.y-opp_head.y) == 2) and (self.points < self.opp_points):
            if head.x < opp_head.x and RIGHT in opts:
                opts.remove(RIGHT)
                opts.append(RIGHT)
            elif head.x > opp_head.x and LEFT in opts:
                opts.remove(LEFT)
                opts.append(LEFT)
            if head.y < opp_head.y and UP in opts:
                opts.remove(UP)
                opts.append(UP)
            elif head.y > opp_head.y and DOWN in opts:
                opts.remove(DOWN)
                opts.append(DOWN)
        for dir in dirstodeath:
            if dir in opts:
                opts.remove(dir)
        if len(opts) == 0:
            opts.append(UP)
        return opts

    def get_opp_moves(self):
        snake = self.snake
        opponent = self.opponent
        apple = self.apple

        head = snake.body[0]
        opp_head = opponent.body[0]
        opts = [LEFT,RIGHT,UP,DOWN]
        dirstodeath = []
        for piece in (snake.body+opponent.body):
            if piece != opp_head:
                if (abs(piece.x-opp_head.x)+abs(piece.y-opp_head.y)) == 1:
                    dirstodeath.append(opp_head.getDirection(piece))
        if opp_head.x == 13:
            dirstodeath.append(RIGHT)
        elif opp_head.x == 0:
            dirstodeath.append(LEFT)
        if opp_head.y == 13:
            dirstodeath.append(UP)
        elif opp_head.y == 0:
            dirstodeath.append(DOWN)
        for dir in dirstodeath:
            if dir in opts:
                opts.remove(dir)
        if len(opts) == 0:
            opts.append(UP)
        return opts

    def snake_move(self,move):
        if self.snake.body[0] == self.apple:
            self.points += 1
            self.apple = Coordinate(6,6)
        self.snake_dead = not self.snake.moveTo(move)

    def opp_move(self,move):
        if self.opponent.body[0] == self.apple:
            self.opp_points += 1
            self.apple = Coordinate(6,6)
        self.opp_dead = not self.opponent.moveTo(move)
        for o_piece in (self.opponent.body+self.snake.body[1:]):
            if o_piece == self.snake.body[0]:
                self.snake_dead |= True
        for s_piece in (self.snake.body+self.opponent.body[1:]):
            if s_piece == self.opponent.body[0]:
                self.opp_dead |= True

class Bot(IBot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.points = 0
        self.opp_points = 0
        self.last_apple = Coordinate(-1,-1)

    def chooseDirection(self, snake: Snake, opponent: Snake, mazeSize: Coordinate, apple: Coordinate) -> Direction:
        #print(self.points,self.opp_points)

        head = snake.body[0]
        opp_head = opponent.body[0]

        if self.last_apple == head:
            self.points += 1
        elif self.last_apple == opp_head:
            self.opp_points += 1

        self.last_apple = apple

        opts = []
        if head.x < apple.x:
            opts.append(RIGHT)
        elif head.x > apple.x:
            opts.append(LEFT)
        if head.y < apple.y:
            opts.append(UP)
        elif head.y > apple.y:
            opts.append(DOWN)
        
        # Blocking the opponent from the apple
        if len(opts) == 2:
            if (apple.x < head.x and head.x < opp_head.x) or (apple.x > head.x and head.x > opp_head.x):
                opts = opts[::-1]
            else:
                random.shuffle(opts)

        dirs = [LEFT,RIGHT,UP,DOWN]
        for dir in dirs:
            if dir not in opts:
                opts.append(dir)

        dirstodeath = []
        for piece in (snake.body+opponent.body):
            if piece != head:
                if (abs(piece.x-head.x)+abs(piece.y-head.y)) == 1:
                    dirstodeath.append(head.getDirection(piece))

        if head.x == 13:
            dirstodeath.append(RIGHT)
        elif head.x == 0:
            dirstodeath.append(LEFT)

        if head.y == 13:
            dirstodeath.append(UP)
        elif head.y == 0:
            dirstodeath.append(DOWN)

        # Avoiding snake collision at apple
        if head.getDirection(apple) != None:
            if opp_head.getDirection(apple) != None:
                if self.points < self.opp_points:
                    opts = opts[::-1]

        # Avoiding potential collision
        if (abs(head.x-opp_head.x)+abs(head.y-opp_head.y) == 2) and (self.points < self.opp_points):
            if head.x < opp_head.x and RIGHT in opts:
                opts.remove(RIGHT)
                opts.append(RIGHT)
            elif head.x > opp_head.x and LEFT in opts:
                opts.remove(LEFT)
                opts.append(LEFT)
            if head.y < opp_head.y and UP in opts:
                opts.remove(UP)
                opts.append(UP)
            elif head.y > opp_head.y and DOWN in opts:
                opts.remove(DOWN)
                opts.append(DOWN)

        for dir in dirstodeath:
            if dir in opts:
                opts.remove(dir)

        if len(opts) == 0:
            return UP
        elif len(opts) == 1:
            return opts[0]

        mxvalue = -1000000000
        bestmove = opts[0]
        for opt in opts:
            board = Board(snake.clone(),opponent.clone(),self.points,self.opp_points,apple)
            board.snake_move(opt)
            value = minimax(1,board,False,-1000000000,1000000000)+(w4*(len(opts)-opts.index(opt)))
            if value >= mxvalue:
                mxvalue = value
                bestmove = opt

        #print(mxvalue)
        return bestmove
