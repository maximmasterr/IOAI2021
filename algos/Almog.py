


from src.bot import IBot
from src.geometry import DOWN, LEFT, RIGHT, UP, Coordinate, Direction
from src.snake import Snake
from collections import deque
import numpy as np

D = DOWN
L = LEFT
R = RIGHT
U = UP 

class Bot(IBot):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.i = 0

	def chooseDirection(self, snake: Snake, opponent: Snake, mazeSize: Coordinate, apple: Coordinate) -> Direction:
		self.i += 1
		arr = np.zeros((mazeSize.x,mazeSize.y),dtype=int)
		
		for c in snake.body:
			arr[c.x][c.y]=10000
		for c in opponent.body:
			arr[c.x][c.y]=10000
		if(len(snake.body)<len(opponent.body)):
			for d in [L,R,U,D]:
				pos = opponent.body[0].moveTo(d)
				if(pos.x<0 or pos.y<0 or pos.x==mazeSize.x or  pos.y==mazeSize.y or arr[pos.x][pos.y]!=0):
					continue
				arr[pos.x][pos.y]=1000
		stk = deque()
		stk.append((1,apple))
		while len(stk)>0:
			all = stk.popleft()
			cur = all[1]
			if(cur.x<0 or cur.y<0 or cur.x==mazeSize.x or  cur.y==mazeSize.y or arr[cur.x][cur.y]!=0):
				continue
			arr[cur.x][cur.y] = all[0]
			for d in [L,R,U,D]:
				stk.append((all[0]+1,cur.moveTo(d)))
		
		numer = []
		for x in range(mazeSize.x):
			for y in range(mazeSize.y):
				if(arr[x][y]!=0):
					if(cur.x==0 or cur.y==0 or cur.x==mazeSize.x-1 or  cur.y==mazeSize.y-1):
						arr[x][y]+=1
					if(cur.x==apple.x and  cur.y==apple.y):
						arr[x][y]=1
					continue
				count = -1-len(numer)
				stk.append(Coordinate(x,y))
				sum = 0
				while len(stk)>0:
					cur = stk.popleft()
					if(cur.x<0 or cur.y<0 or cur.x==mazeSize.x or  cur.y==mazeSize.y or arr[cur.x][cur.y]!=0):
						continue
					arr[cur.x][cur.y] = count
					sum+=1
					for d in [L,R,U,D]:
						stk.append(cur.moveTo(d))
				numer.append(sum)
		for c in snake.body:
			arr[c.x][c.y]=10000
		for c in opponent.body:
			arr[c.x][c.y]=10000
		if(len(snake.body)<len(opponent.body)):
			for d in [L,R,U,D]:
				pos = opponent.body[0].moveTo(d)
				if(pos.x<0 or pos.y<0 or pos.x==mazeSize.x or  pos.y==mazeSize.y or arr[pos.x][pos.y]!=0):
					continue
				arr[pos.x][pos.y]=1000
		#print(arr)
		bestdirect = U
		price = 10000000

		for d in [L,R,U,D]:
			pos = snake.body[0].moveTo(d)
			#print(d)
			#print(pos)
			#print(price)
			if(pos.x<0 or pos.y<0 or pos.x==mazeSize.x or  pos.y==mazeSize.y):
				pass
			elif(arr[pos.x][pos.y]>=0 and arr[pos.x][pos.y]<price):
				price=arr[pos.x][pos.y]
				bestdirect = d
			elif(arr[pos.x][pos.y]<0):
				curprice= 1000-numer[-1-arr[pos.x][pos.y]]
				if(curprice<price):
					price=curprice
					bestdirect = d
		return bestdirect
		