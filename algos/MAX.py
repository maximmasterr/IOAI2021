"""
Unbeateable demon snake
"""
from src.bot import IBot
from src.geometry import Direction, Coordinate, RIGHT, DOWN, UP, LEFT
from src.snake import Snake
import random
import copy
import traceback

MOVES_BEFORE_DIE = 3


def solve_maze(maze, ini, targ):
  maze = copy.deepcopy(maze)
  maze[ini.x][ini.y] = True

  t = []
  for i in range(len(maze)):
    t.append([(320, []) for i in range(len(maze[i]))])
  t[ini.x][ini.y] = (0, [])
  q = [(ini.x, ini.y)]
  while len(q) > 0:
    x, y = q[0]
    q.pop(0)
    s = t[x][y][0]
    if maze[x][y]:
      if maze[x + 1][y] and t[x + 1][y][0] > s + 1:
        t[x + 1][y] = (s + 1, t[x][y][1] + [RIGHT])
        q.append((x + 1, y))

      if maze[x - 1][y] and t[x - 1][y][0] > s + 1:
        t[x - 1][y] = (s + 1, t[x][y][1] + [LEFT])
        q.append((x - 1, y))

      if maze[x][y + 1] and t[x][y + 1][0] > s + 1:
        t[x][y + 1] = (s + 1, t[x][y][1] + [UP])
        q.append((x, y + 1))

      if maze[x][y - 1] and t[x][y - 1][0] > s + 1:
        t[x][y - 1] = (s + 1, t[x][y][1] + [DOWN])
        q.append((x, y - 1))
    else:
      print(x, y, "OH NO")
  return t[targ.x][targ.y]


def long_solve_maze(maze, ini, targ):
  if not maze[targ.x][targ.y]:
    return (320, [])
  maze = maze.copy()
  t = []
  for i in range(len(maze)):
    t.append([(320, []) for i in range(len(maze[i]))])
  t[ini.x][ini.y] = (0, [])
  q = [(ini.x, ini.y)]
  while len(q) > 0:
    x, y = q[0]
    q.pop(0)
    s = t[x][y][0]
    if maze[x][y]:
      if maze[x + 1][y] and t[x + 1][y][0] < s + 1:
        t[x + 1][y] = (s + 1, t[x][y][1] + [RIGHT])
        q.append((x + 1, y))

      if maze[x - 1][y] and t[x - 1][y][0] < s + 1:
        t[x - 1][y] = (s + 1, t[x][y][1] + [LEFT])
        q.append((x - 1, y))

      if maze[x][y + 1] and t[x][y + 1][0] < s + 1:
        t[x][y + 1] = (s + 1, t[x][y][1] + [UP])
        q.append((x, y + 1))

      if maze[x][y - 1] and t[x][y - 1][0] < s + 1:
        t[x][y - 1] = (s + 1, t[x][y][1] + [DOWN])
        q.append((x, y - 1))
    else:
      print(x, y, "OH NO")
  return t[targ.x][targ.y]


def rotate_matrix(m):
  t = []
  for i in range(len(m)):
    t.append([False for i in range(len(m[i]))])
  for y in range(len(m)):
    for x in range(len(m)):
      t[x][y] = m[y][x]
  return t

def generate_map(mazeSize):
  f = []
  for i in range(mazeSize.x + 1):
    f.append([True for i in range(mazeSize.y + 1)])
  return f

def map_snake(f, snakes):
  for s in snakes:
    f[s.head.x][s.head.y] = False
    for b in s.body:
      f[b.x][b.y] = False

  return f

def map_head_moves(f, head):
  f[head.x][head.y + 1] = False
  f[head.x][head.y - 1] = False
  f[head.x + 1][head.y] = False
  f[head.x - 1][head.y] = False
  return f

def map_borders(f, mazeSize):
  for x in range(mazeSize.x + 1):
    f[x][mazeSize.y] = False

  for y in range(mazeSize.y + 1):
    f[mazeSize.x][y] = False
  return f

def map_random_apple(f, apple, chance):
  if not f[apple.x][apple.y] and random.random() < chance:
    f[apple.x][apple.y] = True
  return f

def map_block_apple(f, apple):
  f[apple.x][apple.y] = False
  return f

def print_map(m):
  for x in range(len(m)):
    print(''.join(['_' if m[x][y] else '#' for y in range(len(m[x]))]))


def get_dist(pointA, pointB):
  return ((pointA.x - pointB.x) ** 2 + (pointA.y - pointB.y) ** 2) ** 0.5

def res_direct(p, di):
  if di == UP:
    return (p[0], p[1]+1)
  if di == DOWN:
    return (p[0], p[1]-1)
  if di == RIGHT:
    return (p[0]+1, p[1])
  if di == LEFT:
    return (p[0]-1, p[1])


def shuffled(a):
  random.shuffle(a)
  return a


class Bot(IBot):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def chooseDirection(self, snake: Snake, opponent: Snake,
                      mazeSize: Coordinate, apple: Coordinate) -> Direction:

    # BLOCK Apple seaker
    f = generate_map(mazeSize)
    f = map_snake(f, [snake, opponent])
    f = map_head_moves(f, opponent.head)
    f = map_random_apple(f, apple, 0.2)
    f = map_borders(f, mazeSize)

    #print("+++++++")
    if get_dist(apple, snake.head) - 5 < get_dist(apple, opponent.head): # Why? because I AM GROOT
      ss = solve_maze(f, snake.head, apple)
      if ss[0] < 320:
        return ss[1][0]


    # BLOCK Out seeker
    #print("+++++++")
    f = generate_map(mazeSize)
    f = map_snake(f, [snake, opponent])
    f = map_head_moves(f, opponent.head)
    f = map_block_apple(f, apple)
    f = map_borders(f, mazeSize)

    for p in [(7, 7), (5, 7), (9, 7), (7, 5), (7, 9)]:
      try:
        ss = solve_maze(f, snake.head, Coordinate(*p))
        if ss[0] < 320:
          return ss[1][0]
      except: pass


    # BLOCK Last hope

    #print("+++++++")
    f = generate_map(mazeSize)
    f = map_snake(f, [snake, opponent])
    f = map_head_moves(f, opponent.head)
    f = map_borders(f, mazeSize)
    for d in shuffled([RIGHT, UP, LEFT, DOWN]):
      dp = res_direct((snake.head.x, snake.head.y), d)
      if f[dp[0]][dp[1]]:
        return d

    # # BLOCK Out seeker please
    # f = generate_map(mazeSize)
    # f = map_snake(f, [snake, opponent])
    # f = map_borders(f, mazeSize)

    # print("+++++++")
    # for p in [(7, 7), (5, 7), (9, 7), (7, 5), (7, 9)]:
    #   try:
    #     ss = solve_maze(f, snake.head, Coordinate(*p))
    #     if ss[0] < 320:
    #       return ss[1][0]
    #   except: pass



    # BLOCK I feel death near

    f = generate_map(mazeSize)
    f = map_snake(f, [snake, opponent])
    f = map_borders(f, mazeSize)
    for d in shuffled([RIGHT, UP, LEFT, DOWN]):
      dp = res_direct((snake.head.x, snake.head.y), d)
      if f[dp[0]][dp[1]]:
        return d

    # BLOCK OH NO
    return DOWN
